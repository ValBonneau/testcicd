import pytest

from src import utils

def test_is_prime():
    assert utils.is_prime(4) == False
    assert utils.is_prime(2) == True
    assert utils.is_prime(17) == True
    assert utils.is_prime(-3) == False
    assert utils.is_prime(10) == False
    assert utils.is_prime(7) == True

    if __name__ == '__main__':
        pytest.main()